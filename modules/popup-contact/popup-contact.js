var $ = require('jquery');

function PopupContact(el) {
  var $el = $(el);

  // Will wait for everything on the page to load.
  $('.js-popup-contact').on('click', function(e) {
    e.preventDefault();

    if ($('body').hasClass('is-popup-product-loaded')) {
      $('body').removeClass('is-popup-product-loaded');
    }

    $('body').addClass('is-popup-contact-loaded');
  });

  $(document).click(function(e) {
    //if you click on anything except the modal itself or the "open modal" link, close the modal
    if ($('body').hasClass('is-popup-contact-loaded')) {
      if ($(e.target).closest(".site-header").length) {
        e.preventDefault();
        $("body").removeClass('is-popup-contact-loaded');
      }
    }
  });
}

module.exports = PopupContact;
