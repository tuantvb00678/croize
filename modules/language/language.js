var $ = require('jquery');

function Language(el) {
  var $el = $(el);
  var button = $('.js-language-label');

  button.hover(function() {
    $el.addClass('is-active');
  });

  var languageOption = $('.js-language-option');
  var targetValue = $('.js-language-value');
  var returnTo = $('input[name="return_to"]');
  var returnToValue = returnTo.val();

  languageOption.on('click', function(e) {
    e.preventDefault();
    var value = $(this).attr('data-value');
    targetValue.val(value);
    $('#localization_form').submit();
  });
}

module.exports = Language;
