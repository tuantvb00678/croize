var $ = require('jquery');

function Loading(el) {
  var $el = $(el);

  $('.js-skip').click(function() {
    setTimeout(function() {
      $('.loading').addClass('is-loaded-done');
    }, 300)
  })

  // Will wait for everything on the page to load.
  $(window).bind('load', function() {
    $('.loading, body').addClass('is-loaded');
    setTimeout(function() {
      $('.loading').addClass('is-loaded-done');
    }, 2000)
  });

  // Will remove loading after 1min for users cannnot load properly.
  setTimeout(function() {
    $('.loading, body').addClass('is-loaded');
  }, 8000);

  // scroll

  $('.js-scroll').each(function(){
    var target = $(this).attr('data-target');
    var layoutTarget = $('#' + target);
    if (layoutTarget) {
      $(this).click(function () {
        $('html, body').animate({
          scrollTop: layoutTarget.offset().top - 52,
        });
      });
    }
  });
}

module.exports = Loading;
