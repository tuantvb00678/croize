var $ = require('jquery');

function PopupProduct(el) {
  var $el = $(el);

  // Will wait for everything on the page to load.
  $('.js-popup-product').on('click', function(e) {
    e.preventDefault();

    if ($('body').hasClass('is-popup-contact-loaded')) {
      $('body').removeClass('is-popup-contact-loaded');
    }

    $('body').addClass('is-popup-product-loaded');
  });

  $(document).click(function(e) {
    //if you click on anything except the modal itself or the "open modal" link, close the modal
    if ($('body').hasClass('is-popup-product-loaded')) {
      if ($(e.target).closest(".js-site-header-logo").length) {
        e.preventDefault();
        $("body").removeClass('is-popup-product-loaded');
      }
    }
  });
}

module.exports = PopupProduct;
